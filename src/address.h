/*
 * File:	address.h
 *
 * Author:	Ulli Horlacher (framstag@rus.uni-stuttgart.de)
 *
 * History:	
 *
 *   12 Aug 95   Framstag	initial version
 *    4 Jan 97   Framstag	renamed from destination.h to address.h
 * 				added check_forward()
 *   22 Nov 97   Framstag	added saft2rfc822()
 *
 * Determine own username, recipient username and host and look
 * for an alias in /USERSPOOL/config/aliases and in ~/.elm/aliases.text
 *
 * Copyright � 1995 Ulli Horlacher
 * This file is covered by the GNU General Public License
 */

/*  get recipient user and host */
void destination(int, char **, char *, char *, char *, char *);

/* test if there is an forward address set */
int check_forward(int, char *, char *, char *);

/* look for correct SAFT server and open connection */
int saft_connect(const char *, char *, char *, char *, char *);

/* SAFT URL to RFC822 mail address translation */
int saft2rfc822(char *);
