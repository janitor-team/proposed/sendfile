/*
 * File:	string.h
 *
 * Author:	Ulli Horlacher (framstag@rus.uni-stuttgart.de)
 *
 * History:	
 * 
 *   1995-08-12 Framstag	initial version
 *   1996-02-29	Framstag	added streq and strneq macros
 *   1996-05-05	Framstag	merged streq and strneq
 *   1997-01-16	GNUish		added strerror
 *   1997-02-11	GNUish		include_next replaced
 *   1997-02-23	Framstag	renamed str* functions to str_*
 * 				added str_beq_nocase and str_neq_nocase
 *   2000-12-10 Framstag	added sfgetl() (substitution for fgets())
 *   2005-05-31 Framstag	new snprintf() from <mark.martinec@ijs.si>
 *
 * Header-file of the extended string functions for the sendfile package,
 * which are not found in the standard C library.
 *
 * Copyright � 1995-2005 Ulli Horlacher
 * This file is covered by the GNU General Public License
 */

#include "config.h"
#include <string.h>

/* Try to emulate GNUs include_next here */
/*
#if defined(HAVE_STRING_H)
  #include "/usr/include/string.h"
#else
  int strncasecmp(const char *, const char *, int);
#endif
*/

/* This seems to be true on SunOS 4.1.4 */
#ifndef NULL
  #define NULL (void *) 0
#endif
#if !defined(HAVE_STRERROR)
  char *strerror(int);
#endif

/* trim white spaces */
char *str_trim(char *);

/* transform string to upper case */
char *str_toupper(char *);

/* transform string to lower case */
char *str_tolower(char *);

/* match a simple pattern */
int simplematch(char *, char *, int);

/* string begin equal test */
int str_beq(const char *, const char *);

/* string equal test until length n, ignoring case */
int str_neq_nocase(const char *, const char *, int);

/* string begin equal test, ignoring case */
int str_beq_nocase(const char *, const char *);

/* string equal test */
#define str_eq(s1,s2) (strcmp((s1),(s2)) == 0)
/* #define streq(s1,s2) (strncmp((s1),(s2),(strlen(s1)<strlen(s2))?strlen(s1):strlen(s2))==0) */

#if !defined(HAVE_SNPRINTF) || defined(AIX)
  #define SNPRINTF_LONGLONG_SUPPORT
  #include <stdarg.h>
  #include "snprintf.h"
  /*
  int snprintf(char *buf, size_t len, const char *format,...);
  int vsnprintf(char *buf, size_t len, const char *format, va_list ap);
  */
#else
  /* This is a GNU extention to the libc */
  #if __GLIBC__ > 1
    extern int snprintf __P ((char *, size_t, __const char *, ...));
  #endif
#endif


#define MAXS(s) s,sizeof(s)-1

/* secure version of gets() : read a text line from a stream */
char *sfgetl(char *, int, FILE *);
#define fgetl(s,f) sfgetl(s,sizeof(s),f)
