/*
 * File:	net.h
 *
 * Author:	Ulli Horlacher (framstag@rus.uni-stuttgart.de)
 *
 * Contrib.:	Heiko Schlichting (heiko@fu-berlin.de)
 * 
 * History:	
 * 
 *   12 Aug 95   Framstag	initial version
 *   14 May 96   Framstag	included and modified send_data()
 *   24 Sep 96   Heiko		added get_domainname()
 *   29 Dec 96   Framstag	moved get_domainname to reply.c
 *    9 Dec 97   Framstag	added sock_command()
 *   21 Aug 98	 Framstag	added maximum thruput option for send_data
 *
 * Network routines header-file for the the sendfile client of the
 * sendfile package.
 *
 * Copyright � 1995-1998 Ulli Horlacher
 * This file is covered by the GNU General Public License
 */

/* open socket and connect to client */
#ifdef ENABLE_MULTIPROTOCOL
int open_connection(char *, char *);
#else
int open_connection(char *, int);
#endif

/* get a line from the network socket */
int sock_getline(int, char *);

/* send a line to the network socket */
int sock_putline(int, const char *);

/* send a command line to the network socket and get the answer */
char *sendcommand(int, const char *, char *);

/* get the reply on a command from the server */
char *getreply(int);

/* send a headerline and check the reply code */
int sendheader(int, char *);

/* send file data */
int send_data(int, off_t, const char*, const char*, const char *, 
	      const char *, float, float *);


#ifdef ENABLE_MULTIPROTOCOL
/* default address family */
extern int addressFamily;
#endif
