/*
 * reply - send string conforming to NVT telnet standard
 *
 * INPUT:  rc  - reply code
 *
 * terminates program on fatal error
 *
 * Copyright � 1995 Ulli Horlacher
 * This file is covered by the GNU General Public License
 */

void reply(int);
