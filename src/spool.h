/*
 * File:	spool.h
 *
 * Author:	Ulli Horlacher (framstag@rus.uni-stuttgart.de)
 *
 * History:
 * 
 *   1995-09-01 Framstag	initial version
 *   1996-04-12	Framstag	sign is now in filelist
 * 				sign, comment and fname are now dynamic
 *   1996-04-24	Framstag	added outgoing spool support
 *   1997-02-23	Framstag	extended with TYPE=MIME
 *   1997-09-14	Framstag	moved spoolid() from sendfiled.c to spool.c
 *   1998-11-12	Framstag	outfilelist now contains file size, too
 *   2005-05-31 Framstag	added largefile support
 * 
 * Header-file for functions for operations on files in the sendfile spool
 * directory.
 *
 * Copyright � 1995-2005 Ulli Horlacher
 * This file is covered by the GNU General Public License
 */


/* attribute flags */
#define F_SOURCE        1
#define F_TEXT          2
#define F_MIME          4
#define F_COMPRESS      8
#define F_CRYPT		16
#define F_TAR           32
#define F_EXE           64

#define S_GZIP	"GZIP"
#define S_BZIP2	"BZIP2"


/* list of all files from one sender */
struct filelist { 
  int
    id,				/* id number */
    flags;			/* binary, source, text, compress and tar flag */
  off_t
    osize,			/* original size */
    csize,			/* compressed size */
    tsize;			/* transfered size */
  time_t
    rtime;			/* receiving date */
  char
    rdate[DLEN+1],		/* receiving date */
    date[DLEN+1],		/* file date */
    charset[DLEN+1],		/* character set name */
    compress[DLEN+1],		/* compression method */
    *sign,			/* pgp signature */
    *comment,			/* file comment (ISO Latin-1) */
    *fname;			/* UTF-7 file name */
  struct filelist *next;	/* next list element */
};

/* list of all senders */
struct senderlist { 
  char from[MAXLEN];		/* sender */
  struct senderlist *next;	/* next list element */
  struct filelist *flist;	/* list of files */
};

/* list of all files from to a single host */
struct outfilelist { 
  struct outfilelist *next;	/* next list element */
  char
    *to,			/* recipient name */
    *from,			/* sender name */
    *fname,			/* file name (UTF-7) */
    *oshfn;			/* outgoing spool header file name */
  off_t size;			/* size */
};

/* list of hosts for outgoing spool */
struct hostlist { 
  struct hostlist *next;	/* next list element */
  char host[MAXLEN];		/* recipient host */
  struct outfilelist *flist;	/* list of files */
};

#ifdef deadcode
/* rest list of spool files (from found forward addresses) */
struct restlist { 
  struct restlist *next;	/* next list element */
  char
    to[FLEN],			/* recipient name */
    from[FLEN],			/* sender name */
    oshfn[MAXLEN],		/* outgoing spool header file name */
    host[MAXLEN];		/* recipient host */
};
#endif


/* send string conforming to NVT standard */
void out(const char *);

/* scan the spool directory and create structure lists */
struct senderlist *scanspool(char *);

/* scan the outgoing spool directory and create structure lists */
struct hostlist *scanoutspool(char *);

/* create new sender list element and fill it out */
struct senderlist *newsle(struct filelist *, const char *);

/* create new host list element and fill it out */
struct hostlist *newhle(struct outfilelist *, const char *);

/* delete a spool file */
int delete_sf(struct filelist *, int);

/* get the next spool id number */
int spoolid(int);
