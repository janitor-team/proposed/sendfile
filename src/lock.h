/*
 * File:        lock.h
 *
 * Author:      Ulli Horlacher (framstag@belwue.de)
 *
 * History:
 *
 *   1998-05-10	Framstag	moved from sendfiled.c
 *
 * These are functions which sets or tests advisory locks conforming to POSIX 
 * fcntl() call.
 *
 * Copyright � 1998 Ulli Horlacher
 * This file is covered by the GNU General Public License
 */

/* write-lock a file */
int wlock_file(int);

/* test the lock status of a file */
int tlock_file(int);
