			Sendfile / SAFT


Sendfile stellt einen asynchronen Filetransfer-Service fuer das Internet
dar, wie es ihn beispielsweise im Bitnet gibt: Ein Benutzer A kann einem
beliebigen Benutzer B Dateien zuschicken, ohne dass B dabei aktiv werden
muss.

Der bereits vorhandene Standard-Filetransfer, das ftp, ist ein synchroner
Dienst: Der Benutzer muss sowohl auf Sender- als auch auf Empfaengerseite
ueber einen Account verfuegen.

Ein benutzerbezogener Datenaustausch ueber anonymous ftp-Server ist
umstaendlich und sicherheitstechnisch indiskutabel. Das Versenden von
Files via Mail erfolgt zwar asynchron, ist aber ebenfalls umstaendlich
(Binaerdateien muessen vor dem Versenden speziell codiert und nach dem
Empfangen entsprechend decodiert werden) und fuer die Uebertragung
groesserer Datenmengen ungeeignet. Bei beiden Verfahren gehen ueberdies
fast alle Fileattribute verloren.

Sendfile fuer Unix, eine Implementation des SAFT-Protokolls (Simple
Asynchronous File Transfer), bietet jetzt auch im Internet einen
vollwertigen asynchroner Filetransfer-Service. Das SAFT-Protokoll wird in
naechster Zukunft als RFC eingereicht.


Sendfile ist eine Client/Server Applikation und umfasst die Teile:

  - sendfiled : der Server (Daemon)

  - sendfile  : ein Client zum Verschicken von Dateien

  - sendmsg   : ein Client zum Verschicken von einzeiligen Nachrichten

  - receive   : ein Client zum Abholen von empfangenen Dateien
  
  - Dokumentation : LIESMICH-Files fuer schnelle Uebersicht, Unix-man-pages
                    und eine ausfuehrliche Protokoll- und Programmbeschreibung.

Der sendfile-Client ist ein Userprogramm, das Dateien an den sendfiled-
Server des Empfaengersystems verschickt. Der sendfiled nimmt die Dateien
entgegen, legt sie im lokalen Spoolbereich ab und informiert den
Empfaenger. Der Empfaenger kann sich dann die Dateien mittels des
receive-Clients in sein Directory kopieren, wobei das Original aus dem
Spool geloescht wird.

Beispiele fuer den sendfile-Client:

  $ sendfile doku.ps zrxh0370

  $ sendfile -a Sourcen *.f90 Makefile uranus@bigvax.inka.de

Sendfile komprimiert automatisch vor dem Versenden und spart so
Netzbandbreite. Ausserdem ist es moeglich, Directories oder mehrere Files
als ein Archivfile zu verschicken (siehe zweites Beispiel oben). Eine
Verschluesselung und/oder Signierung (digital signature) der Files ist
durch integrierten pgp-Support moeglich.

Der sendmsg-Client ist ein Userprogramm, das einzeilige Textnachrichten an
den sendfiled verschickt, welcher sie dann direkt auf das Terminal des
Empfaengers schreibt.

Beispiele fuer den sendmsg-Client:

  $ sendmsg framstag@linux
  message: Pizza ist fertig! Essen kommen!

Die Adressen bei sendfile und sendmsg muessen real existierende Internet-
Accounts darstellen und duerfen keine generischen Mail-Adressen
(sogenannte mx-Records) sein.

Zum Abholen aus dem Spool benutzt man den receive-Client. Beispiel:

  $ receive -l

  From zrxh0370@baracke.rus.uni-stuttgart.de (Ulli Horlacher)
  -----------------------------------------------------------
    1) 1995-Aug-10 15:41:24      3 KB  LIESMICH
    2) 1995-Aug-10 15:41:37     30 KB  doku.txt
    3) 1995-Aug-10 15:42:09    113 KB  Sourcen (archive)

  $ receive LIESMICH
  %receive-I, LIESMICH received


Sendfile laeuft bisher auf AIX, BSDI, Convex-OS, Digital Unix, FreeBSD,
HP-UX, IRIX, Linux, NeXTstep/Mach, OSF/1, SunOS 4, SunOS 5 (Solaris) und
Ultrix. Portierungen fuer Windows NT und OS/2 sind in Vorbereitung.

Sendfile benoetigt den gcc-Compiler zum Uebersetzen und gzip zur Laufzeit,
sowie optional GNU recode und pgp.

Der sendfiled Server muss von root installiert werden, weil er ein
privilegierter Internetdaemon ist, die Clients koennen als normale
qUserprogramme betrieben werden. Die Installation kann entweder ueber ein
automatisches Skript (no questions, no answers) oder manuell anhand einer
ausfuehrlichen Anleitung erfolgen.

Sicherheitstechnisch ist sendfile unbedenklich, da die Clients als normale
Userprogramme laufen und der Server nur vom inetd kontrolliert aufgerufen
wird. Der sendfiled schreibt ausschliesslich in das Userspool-Directory
und dies auch nur mit der UID des Empfaengers. Der Administrator kann eine
Datei anlegen mit lokalen Usern, die vom sendfile-Service ausgeschlossen
sind und auch die Groesse des Spool beschraenken. Der User selbst kann
eine Datei anlegen von Absendern, von denen er nichts empfangen moechte.

Die Kommunikation erfolgt ueber den tcp-Port 487. Dieser ist von der IANA
(Internet Assigned Numbers Authority) fuer SAFT reserviert worden:
ftp://ftp.isi.edu/in-notes/iana/assignments/port-numbers

Autor ist Ulli Horlacher (framstag@rus.uni-stuttgart.de).

URLs:	ftp://ftp.uni-stuttgart.de/pub/unix/comm/sendfile/sendfile.tar.gz
	http://www.belwue.de/saft/index.html
