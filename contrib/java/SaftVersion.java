/**
 * Diese Klasse testet die verwendete sendfiled-Version eines
 * angegebenen Servers.
 * <p>
 * Sollte man vielleicht nicht zu oft aufrufen. Einige beobachten
 * ihre Logfiles sehr genau.
 * 
 * @author Stefan Scholl <stesch@sks.inka.de>
 * @version $Id$
 */

import java.net.*;
import java.io.*;


public class SaftVersion {
   static final int SAFTPORT = 487;
   
   public static void main( String argv[] ) {
      if( argv.length > 0 ) {
	 
	 try {
	    String line, version = null;
	    Socket sock = new Socket( argv[0], SAFTPORT);
	    DataInputStream sdin = new DataInputStream(sock.getInputStream());
	    PrintStream spout = new PrintStream(sock.getOutputStream());
	    
	    spout.println("version");
	    spout.println("quit");
	    
	    do {
	       line = sdin.readLine();
	       if( line.startsWith("215") )
		 version = line.substring(4);
	    } while ( !line.startsWith("221"));
	    
	    if( version != null ) {
	       System.out.println(argv[0] + "'s SAFT:");
	       System.out.println(version);
	    } else {
	       System.out.println("No Version found!?");
	    }
	    
	 }
	 catch ( UnknownHostException e ) {
	    System.out.println("Can't find " + argv[0]);
	 }
	 catch ( IOException e ) {
	    System.out.println("Error connecting to " + argv[0]);
	 }
	 
      } else {
	 System.out.println("USAGE: java SaftVersion.class <hostname>");
      }
   }
}
