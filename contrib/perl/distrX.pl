#! /usr/bin/perl

# distrX.pl 199703062030

# Example of a sendfiled post processing filter 
# =======
# using the extended header feature of sendfile
#
#
# Insert the following line to /usr/local/etc/sendfile.aliases:
#
# distrXtest   <username> | <path>/distrX.pl
#
#
# Use the right strings instead of <username> and <path>. Modify the
# distribution list, $sender and $realname in the global section.
#

# History
#
# * 1997-03-31T13:25+02 Stefan Scholl <stesch@sks.inka.de
#   - New version of readsaftheadraw()
# * 1997-03-06T21:45+01 Stefan Scholl <stesch@sks.inka.de>
#   - First version with extended header feature
#   - POSIX::tmpnam()
# * 1997-03-06T21:30+01 Stefan Scholl <stesch@sks.inka.de>
#   - Some minor code cleanup
#   - read() instead <STDIN> in read-loop
# * 1997-03-03T20:26+01 Stefan Scholl <stesch@sks.inka.de>
#   - Use the module Unicode::Utf7
#   - Insert comment (and forward the old one, too)
# * 1997-02-01T16:45+01 Stefan Scholl <stesch@sks.inka.de>
#   - First version

use Unicode::Utf7 qw( isotoutf7 utf7toiso );
use POSIX;


#-globals---------------------------------------------------

# SAFT-Urls for all receivers
@distlist = qw( saft://parsec.inka.de/stesch saft://bofh.belwue.de/framstag );

# Don't forget to set the right from
$sender       = 'Distributor';
$realname     = 'Test Testerson';

$sendfilepath = '/usr/local/bin/sendfile';
$PACKET       = 512;
$CRLF         = "\x0d\x0a";
#-----------------------------------------------------------


# Sideeffect: result is in %shead

sub readsaftheadraw {
    my $l;

    while($l = <STDIN>) {
        my ($key, $val);
	
	$l =~ tr/\r\n//d; # get rid of CRs and LFs.
        ($key, $val) = split(/\s+/, $l, 2);
	return if uc($key) eq 'DATA';
	$shead{uc($key)} = $val;
    }
}


#-----------------------------------------------------------


#-main------------------------------------------------------


# Reading the header into %shead. Following data on STDIN is
# the transfered file.
&readsaftheadraw;

foreach $k (keys %shead) {
    print ">$k< : >$shead{$k}<\n";
}

# Comment
if(defined($shead{'COMMENT'})) {   # There's allready a comment
    $comment = isotoutf7('forward from ' . utf7toiso($shead{FROM})) .
	       isotoutf7($CRLF) .
	       $shead{'COMMENT'};
} else {
    $comment = isotoutf7('forward from ') . $shead{FROM};
}

# FROM
if(defined($realname)) {
    $from = isotoutf7($sender) . ' ' . isotoutf7($realname);
} else {
    $from = isotoutf7($sender);
}

# Name of tempfile
$outf = tmpnam(0);

if(open(OUT, ">$outf")) {
    my $data;
    
    while(read(STDIN, $data, $PACKET)) {
        print OUT $data;
    }
    close(OUT);
    
    # Now we have the data of the received file in $outf.
    # No matter if it's compressed or not. It's the raw data.
    
    
    # Time to distribute it.
    
    foreach $receiver (@distlist) {
        my($host, $to) = ($receiver =~ /^saft:\/\/(.*?)\/(.*)/);
	
	open(DF, $outf) || die "Can't open $outf\n";

        if(open(SF, "|$sendfilepath -X $host")) {
	    my $data; 
	    
	    print SF "FROM $from" . $CRLF;
	    print SF "TO " . isotoutf7($to) . $CRLF;
	    print SF "COMMENT $comment" . $CRLF;

	    foreach $key (keys %shead) {
	        next if $key eq 'FROM';
		next if $key eq 'TO';
		next if $key eq 'COMMENT';
	        
		print SF "$key $shead{$key}" . $CRLF;
	    }
	    
	    print SF "DATA" . $CRLF;
	    
	    while(read(DF, $data, $PACKET)) {
  	        print SF $data;
	    }
	    
	    close(SF);
	    
	} else {
	    # Errors suck!
	}
	close(DF);
    }
    
    # get rid of it!
    unlink($outf);
    
} else {
    # right place for error logging. :-)
}

#-----------------------------------------------------------
